var fs = require('fs');
var path = require('path');



var file_path = path.join(__dirname,"data.db"); // build the complete database path




if(fs.existsSync(file_path)){
  let dictionary = JSON.parse(fs.readFileSync(file_path)); //store dictionary data in variable called dictionary
  let key=process.argv[3];
  let val=process.argv[4];
  let comm=process.argv[2];
  switch (comm) {
    case "list" :
     console.log(dictionary);
      break;
    case "add" :  
    dictionary[key] = val || "";
      break;
    case "get" : 
    if (key) {
      console.log(dictionary[key]?dictionary[key] : 'The key not found');
    } else {
      console.log('Invalid Inputs , Please Enter Key');
    }
  //  console.log(dictionary[key]);
      break;
    case "remove" : 
    if (key) {
      delete dictionary[key];
  } else {
      console.log('Invalid Inputs , Please Enter Key');
  }

      break;
    case "clear" : 
    dictionary = {};
        break;
    default:
        console.log("Unknown, please enter one of the following instructions:list,add,get,remove,clear");
  }
  fs.writeFileSync(file_path,JSON.stringify(dictionary));
}else{
  console.log("not found");
}


