FROM node:carbon

# Create app directory
WORKDIR /usr/src/app


# Bundle app source
COPY . .

EXPOSE 8080
#CMD [ "node", "store.js list" ]

